# Host

https://cart-project-110cf.web.app/

# Instalation

You can install the app by using a command npm i && npm run start

# ReduxCart

It's a simple shop application using API to fetch the items available to order.

# Login
The first you will see is the login form. It's not working the way it should now, but it still has some validation.
You enter any fake but valid email (it should contain '@' and have some symbols after it), and any password that is at least 6 symbols and you're in.

# Shop
If you're logged in successfully, you will see some extra navigation buttons, which are useless now. 
There is a logout button, which will log you out and send you back to the Login form.

The main page contains around 15 different items fetched with a Fakestore API. 
You can add any of them to the cart with an 'Add' button. 
You can toggle the cart with the Cart button. It also reflects the total amount of items you have in your cart if it's closed.

You can open a cart to see the amount of each of the ordered items, the total summary and the total quantity.

# Order
If you press the 'Order' button, an order form will open. It also has some validation. If you enter an incorrect value, the app will show you a notification with a note saying how to fix it.
The submit button will stay disabled until all fields are filled in correctly.

# Notification
When the form is submitted, the app sends a request to a Firebase database and saves both user's data (filled in in the order form) and the cart items.
You will get notified via a modal window in case of a successful request. You can close the modal window by clicking anywhere on your screen.
The cart will be empty, the order form will close automatically after the submission.

# Screenshots
### Login form
![login form](./screenshots/cart.PNG)
### Form validation
![Form validation](./screenshots/cart1.PNG)
### Store
![Store](./screenshots/cart2.PNG)
### Cart with items
![Cart with items](./screenshots/cart3.PNG)
### Order form
![Order form](./screenshots/cart4.PNG)
### Success modal window
![Success modal window](./screenshots/cart5.PNG)
### Cleared cart after making an order
![Cleared cart after making an order](./screenshots/cart6.PNG)
