import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Navigate, Route, Routes, useNavigate } from 'react-router-dom';
import './App.css';
import { Header } from './components/Header/Header';
import { fetchItems } from './components/store/market-actions';
import { CartPage } from './pages/CartPage';
import { LoginPage } from './pages/LoginPage';
import { MarketPage } from './pages/MarketPage';
import { OrderPage } from './pages/OrderPage';

function App() {
  const dispatch = useDispatch();
  const isAuth = useSelector(state => state.auth.isAuth);

  useEffect(() => {
    if (isAuth) {
      dispatch(fetchItems());
    }
  }, [dispatch, isAuth]);

  return (
    <div className="App">
      <Header />
      <Routes>
        <Route path='/' element={<Navigate to='/login' />} />
        <Route path='/login' element={<LoginPage />} />
        <Route path='/cart' element={<CartPage />} />
        <Route path='/market' element={<MarketPage />} />
        <Route path='/order' element={<OrderPage />} />
      </Routes>
      <main>
        <div className="gradient-bg-1"></div>
      </main>
    </div>
  );
}

export default App;
