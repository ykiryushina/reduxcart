import { useState } from "react"

export const useData = () => {
    const [inputData, setInputData] = useState({});

    const getData = (data) => {
        setInputData(data);
    }

    return {
        inputData,
        getData
    }
}