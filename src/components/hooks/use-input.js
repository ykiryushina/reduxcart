import { useState } from 'react';

export const useInput = (validValue,) => {
    const [enteredValue, setEnteredValue] = useState('');
    const [isTouched, setIsTouched] = useState(false);

    const valueIsValid = validValue(enteredValue);
    const hasError = !valueIsValid && isTouched;

    const onValueChangeHandler = (e) => {
        setEnteredValue(e.target.value);
    };

    const onInputBlurHandler = (e) => {
        setIsTouched(true);
    };

    const reset = () => {
        setEnteredValue('');
        setIsTouched(false);
    };

    return {
        enteredValue,
        hasError,
        onValueChangeHandler,
        onInputBlurHandler,
        reset,
        isValid: valueIsValid
    }
}