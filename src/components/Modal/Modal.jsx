import { createPortal } from 'react-dom';
import s from './Modal.module.css';

export const Backdrop = ({ onClose }) => {
    return (
        <div className={s.backdrop} onClick={onClose}></div>
    )
}

export const ModalOverlay = ({ children }) => {
    return (
        <div className={s.modal}>{children}</div>
    )
}

const portalElement = document.getElementById('overlay');

export const Modal = ({ children, onClose }) => {
    return (
        <>
            {createPortal(<Backdrop onClose={onClose} />, portalElement)}
            {createPortal(<ModalOverlay>{children}</ModalOverlay>, portalElement)}
        </>
    )
}