import { useDispatch } from 'react-redux';
import { cartActions } from '../../store/cart-slice';
import s from './CartItem.module.css';

export const CartItem = ({ title, price, quantity, totalItemPrice, id }) => {
    const dispatch = useDispatch();

    const addItemHandler = () => {
        dispatch(cartActions.addItemToCart({
            id,
            title,
            price
        }));
    }

    const removeItemFromCart = () => {
        dispatch(cartActions.removeItemFromCart(id));
    }

    return (
        <div className={s['cart-item']}>
            <div className={s.title}>{title}</div>
            <div className={s['total-price']}>{totalItemPrice}$</div>
            <div className={s.info}>
                <div className={s.quantity}>x{quantity}</div>
                <div className={s['price-per-st']}>{price}$/st.</div>
            </div>
            <div className={s.actions}>
                <button className={s.item__btn} onClick={removeItemFromCart} >-</button>
                <button className={s.item__btn} onClick={addItemHandler}>+</button>
            </div>
        </div>
    )
}