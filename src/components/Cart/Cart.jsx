import { Card } from '../Card/Card';
import s from './Cart.module.css';
import { CartItem } from './CartItem/CartItem';
import { Button } from '../Button/Button';
import { useDispatch, useSelector } from 'react-redux';
import { useCallback } from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

export const Cart = () => {
    const [isCartEmpty, setIsCartEmpty] = useState(true);
    const dispatch = useDispatch();
    const cart = useSelector(state => state.cart);
    const navigate = useNavigate();

    const onOrderHandler = useCallback(() => {
        navigate('/order');
    }, [navigate]);

    useEffect(() => {
        if (cart.items.length !== 0) {
            setIsCartEmpty(false);
        } else {
            setIsCartEmpty(true);
        }
    }, [cart.items, dispatch]);

    return (
        <Card>
            <div className={s.cart}>
                <div className={s.title}>Your Cart</div>
                <div className={s['cart-items']}>
                    {cart.items.map(item => (
                        <CartItem
                            id={item.id}
                            key={item.id}
                            title={item.title}
                            price={item.price}
                            quantity={item.quantity}
                            totalItemPrice={item.totalItemPrice}
                        />
                    ))}
                </div>
                <div className={s.totals}>
                    <div className={s['total-cost']}>
                        <div className={s['cost-title']}>Total cost</div>
                        <div className={s.sum}>{cart.totalSum}$</div>
                    </div>
                    <div className={s['total-amount']}>
                        <div className={s['amount-title']}>Total amount</div>
                        <div className={s.amount}>{cart.totalQuantity} st.</div>
                    </div>
                </div>
                <div className={s.actions}>
                    <Button text={'Order'} onClick={onOrderHandler} disabled={isCartEmpty} />
                </div>
            </div>
        </Card>
    )
}