import { createSlice } from "@reduxjs/toolkit";

export const marketSlice = createSlice({
    name: 'market',
    initialState: {
        items: [],
        isLoading: false
    },
    reducers: {
        setItems(state, action) {
            state.items = action.payload;
        },
        setIsLoading(state) {
            state.isLoading = !state.isLoading;
        }
    }
});

export const marketAcitions = marketSlice.actions;