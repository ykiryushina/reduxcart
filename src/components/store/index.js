import { configureStore } from '@reduxjs/toolkit';
import { authSlice } from './auth-slice';
import { cartSlice } from './cart-slice';
import { marketSlice } from './market-slice';

export const store = configureStore({
    reducer: {
        auth: authSlice.reducer,
        cart: cartSlice.reducer,
        market: marketSlice.reducer,
    }
})