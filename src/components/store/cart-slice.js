import { createSlice } from "@reduxjs/toolkit";

export const cartSlice = createSlice({
    name: 'cart',
    initialState: {
        items: [],
        totalQuantity: 0,
        totalSum: 0,
    },
    reducers: {

        addItemToCart(state, action) {
            const newItem = action.payload;
            const existingItem = state.items.find(item => item.id === newItem.id);
            state.totalQuantity++;
            if (!existingItem) {
                state.items.push({
                    id: newItem.id,
                    title: newItem.title,
                    price: newItem.price,
                    desc: newItem.desc,
                    quantity: 1,
                    totalItemPrice: newItem.price * 1
                })
                state.totalSum = parseFloat((state.totalSum + newItem.price).toFixed(2));
            } else {
                existingItem.quantity++;
                existingItem.totalItemPrice = parseFloat((existingItem.price * existingItem.quantity).toFixed(2));
                state.totalSum = parseFloat((state.totalSum + existingItem.price).toFixed(2));
            }
        },

        removeItemFromCart(state, action) {
            const existingItem = state.items.find(item => item.id === action.payload);
            if (existingItem.quantity === 1) {
                state.items = state.items.filter(item => item.id !== existingItem.id);
            }
            existingItem.totalItemPrice = parseFloat((existingItem.totalItemPrice - existingItem.price).toFixed(2));
            existingItem.quantity--;
            state.totalQuantity--;
            state.totalSum = parseFloat((state.totalSum - existingItem.price).toFixed(2));
        },

        cleanCart(state) {
            state.items = [];
            state.totalQuantity = 0;
            state.totalSum = 0;
        }
    }
});

export const cartActions = cartSlice.actions;