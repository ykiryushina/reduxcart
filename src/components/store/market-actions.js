import { marketAcitions } from "./market-slice";

export const fetchItems = () => {
    return async (dispatch) => {
        const fetchData = async () => {
            dispatch(marketAcitions.setIsLoading());
            const response = await fetch('https://fakestoreapi.com/products', {
                method: 'GET',
            })
            dispatch(marketAcitions.setIsLoading());
            return await response.json();
        }

        try {
            const marketData = await fetchData();
            dispatch(marketAcitions.setItems(marketData || []))
        } catch (error) {
            console.log(error);
        }
    }
}