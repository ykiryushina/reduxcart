import s from './Loader.module.css';

export const Loader = () => {
    return (
        <div className={s['spinner-wrapper']}>
            <div className={s.spinner} />
        </div>
    )

}