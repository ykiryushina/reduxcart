
import { Button } from '../../Button/Button';
import s from './MarketItem.module.css';
import { useDispatch } from 'react-redux';
import { cartActions } from '../../store/cart-slice';

export const MarketItem = ({ id, title, price, img, reviews, rate, category, desc }) => {
    const dispatch = useDispatch();

    const onAddHandler = () => {
        dispatch(cartActions.addItemToCart({
            id,
            title,
            price
        }))
    }

    return (
        <div className={s.item}>
            <div className={s['item-data']}>
                <div className={s['img-wrapper']}>
                    <img src={img} alt="item image" className={s.img} />
                </div>
                <div className={s.info}>
                    <div className={s.title}>{title}</div>
                    <div className={s.category}>{category}</div>
                    <div className={s.desc}>{desc}</div>
                    <div className={s.rate}>Rate: {rate}</div>
                    <div className={s.reviews}>Total reviews: {reviews}</div>
                </div>
            </div>
            <div className={s.info}>
                <div className={s.price}>{price}$</div>
                <Button text={'Add'} onClick={onAddHandler} />
            </div>
        </div>
    )
}