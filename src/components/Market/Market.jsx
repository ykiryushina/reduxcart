import { useSelector } from 'react-redux';
import { Card } from '../Card/Card';
import { Loader } from '../Loader/Loader';
import s from './Market.module.css';
import { MarketItem } from './MarketItem/MarketItem';

export const Market = () => {
    const items = useSelector(state => state.market.items);
    const isLoading = useSelector(state => state.market.isLoading);

    return (
        <Card>
            {isLoading && <Loader />}
            <div className={s['items-wrapper']}>
                {items.map(item => (
                    <MarketItem
                        key={item.id}
                        id={item.id}
                        title={item.title}
                        price={item.price}
                        rate={item.rating.rate}
                        reviews={item.rating.count}
                        img={item.image}
                        category={item.category}
                        desc={item.description}
                    />
                ))}
            </div>
        </Card>
    )
}