import { useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { Button } from '../../Button/Button';
import { Card } from '../../Card/Card';
import { useData } from '../../hooks/use-data';
import { Modal } from '../../Modal/Modal';
import { cartActions } from '../../store/cart-slice';
import { FormInputsWrapper } from '../FormInputsWrapper/FormInputsWrapper';
import { Input } from '../Input/Input';
import s from './OrderForm.module.css';

export const OrderForm = () => {
    const [isSent, setIsSent] = useState(false);
    const cartItems = useSelector(state => state.cart.items);
    const dispatch = useDispatch();
    let isFormValid = false;
    const { inputData: nameInputData, getData: getNameInputData } = useData();
    const { inputData: cityInputData, getData: getCityInputData } = useData();
    const { inputData: streetInputData, getData: getStreetInputData } = useData();
    const { inputData: houseInputData, getData: getHouseInputData } = useData();
    const { inputData: floorInputData, getData: getFloorInputData } = useData();
    const { inputData: apartmentInputData, getData: getApartmentInputData } = useData();
    const { inputData: commentsInputData, getData: getCommentsInputData } = useData();
    const navigate = useNavigate();

    const requestBody = {
        nameInputData: nameInputData.enteredValue,
        cityInputData: cityInputData.enteredValue,
        streetInputData: streetInputData.enteredValue,
        houseInputData: houseInputData.enteredValue,
        floorInputData: floorInputData.enteredValue,
        apartmentInputData: apartmentInputData.enteredValue,
        commentsInputData: commentsInputData.enteredValue,
        cartItems: cartItems
    };

    const makeOrder = async () => {
        await fetch('https://cart-project-110cf-default-rtdb.europe-west1.firebasedatabase.app/orders.json', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(requestBody)
        })
    };

    const sumbitHandler = (e) => {
        e.preventDefault();
        if (!isFormValid) {
            return;
        }
        makeOrder();
        nameInputData.reset();
        cityInputData.reset();
        streetInputData.reset();
        houseInputData.reset();
        floorInputData.reset();
        apartmentInputData.reset();
        commentsInputData.reset();
        setIsSent(true);
        dispatch(cartActions.cleanCart());
    };

    if (
        nameInputData.isValid &&
        cityInputData.isValid &&
        streetInputData.isValid &&
        houseInputData.isValid &&
        floorInputData.isValid &&
        apartmentInputData.isValid &&
        commentsInputData.isValid
    )
        isFormValid = true;

    const onCancelHandler = useCallback(() => {
        navigate('/cart');
    }, [navigate]);

    const onModalClose = () => {
        setIsSent(false);
        navigate('/market', { replace: true });
    }

    return (
        <>
            {isSent && <Modal onClose={onModalClose}>Your order is successfully sent!</Modal>}
            <Card>
                <form className={s.form} onSubmit={sumbitHandler}>
                    <div className={s.title}>Order form</div>
                    <FormInputsWrapper>
                        <Input label={'Name'} id={'name'} getInputData={getNameInputData} />
                        <Input label={'City'} id={'city'} getInputData={getCityInputData} />
                        <Input label={'Street'} id={'street'} getInputData={getStreetInputData} />
                        <Input label={'House'} id={'house'} getInputData={getHouseInputData} />
                        <Input label={'Floor'} id={'floor'} getInputData={getFloorInputData} />
                        <Input label={'Apartment'} id={'apartment'} getInputData={getApartmentInputData} />
                        <Input label={'Comments'} id={'comments'} getInputData={getCommentsInputData} />
                    </FormInputsWrapper>
                    <div className={s.actions}>
                        <Button text={'Cancel'} onClick={onCancelHandler} />
                        <Button text={'Submit'} disabled={!isFormValid} />
                    </div>
                </form>
            </Card>
        </>
    )
}