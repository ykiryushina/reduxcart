import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { Button } from '../../Button/Button';
import { Card } from '../../Card/Card';
import { useData } from '../../hooks/use-data';
import { authActions } from '../../store/auth-slice';
import { FormInputsWrapper } from '../FormInputsWrapper/FormInputsWrapper';
import { Input } from '../Input/Input';
import s from './LoginForm.module.css';


export const LoginForm = () => {
    const { inputData: emailData, getData: setEmailData } = useData();
    const { inputData: passwordData, getData: setPasswordData } = useData();
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const sumbitHandler = (e) => {
        e.preventDefault();
        if (!formIsValid) {
            return;
        }
        dispatch(authActions.login());
        navigate('/market');
        emailData.reset();
        passwordData.reset();
    }

    let formIsValid = false;

    if (emailData.isValid && passwordData.isValid) formIsValid = true;

    return (
        <Card>
            <form className={s.form} onSubmit={sumbitHandler}>
                <div className={s.title}>Login form</div>
                <FormInputsWrapper>
                    <Input label={'Email'} id={'email'} getInputData={setEmailData} />
                    <Input label={'Password'} id={'password'} getInputData={setPasswordData} />
                </FormInputsWrapper>
                <div className={s.actions}>
                    <Button text={'Submit'} disabled={!formIsValid} />
                </div>
            </form>
        </Card>
    )
}