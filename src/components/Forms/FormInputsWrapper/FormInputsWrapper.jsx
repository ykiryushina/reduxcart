import s from './FormInputsWrapper.module.css';

export const FormInputsWrapper = ({ children }) => {
    return (
        <div className={s['form__inputs-wrapper']}>{children}</div>
    )
}