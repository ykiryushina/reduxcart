import { memo, useEffect } from 'react';
import { useInput } from '../../hooks/use-input';
import s from './Input.module.css';

export const Input = memo(({ label, id, getInputData }) => {
    let errorMessage;
    const inputValidation = (value) => {
        switch (label) {
            case 'Email':
                return value.includes('@');
            case 'Password':
                return value.length >= 6;
            case 'Name':
                return value.length >= 2;
            case 'City':
                return value.length >= 2;
            case 'Comments':
                return value.length === 0 || value.length > 0;
            default:
                return value.length >= 1;
        }
    }

    const getErrorMessage = () => {
        if (label === 'Email') {
            return errorMessage = 'Enter a valid email';
        } else if (label === 'Password') {
            return errorMessage = 'The password length must be at least 6 digits';
        } else if (label === 'Name') {
            return errorMessage = 'Enter a valid name';
        } else if (label === 'City') {
            return errorMessage = 'Enter a valid city name';
        } else if (label === 'Street') {
            return errorMessage = 'Enter a valid street name';
        } else {
            return errorMessage = 'The length must be at least one symbol';
        }
    }

    const {
        enteredValue,
        isValid,
        hasError,
        onValueChangeHandler,
        onInputBlurHandler,
        reset
    } = useInput(inputValidation, getErrorMessage);

    useEffect(() => {
        getInputData({ enteredValue, isValid, reset });
    }, [enteredValue, isValid]);

    if (hasError) {
        getErrorMessage();
    }

    const inputClasses = hasError ? `${s.control} ${s.invalid}` : `${s.control}`;

    return (
        <div className={inputClasses}>
            <label htmlFor={id} className={s.label}>{label}</label>
            <input
                className={s.input}
                id={id}
                type={id}
                value={enteredValue}
                onChange={onValueChangeHandler}
                onBlur={onInputBlurHandler}
            />
            {hasError && <p className={s.error}>{errorMessage}</p>}
        </div>
    )
}) 