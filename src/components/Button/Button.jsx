import { memo } from 'react';
import s from './Button.module.css';

export const Button = memo(({ text, onClick, disabled }) => {
    return <button className={s.button} onClick={onClick} disabled={disabled}>{text}</button>
}) 