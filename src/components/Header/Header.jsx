import { useCallback } from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { memo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button } from '../Button/Button';
import { authActions } from '../store/auth-slice';
import s from './Header.module.css';
import './cart-animation.css';
import { Link, useNavigate } from 'react-router-dom';

export const Header = memo(() => {
    const [animation, setAnimation] = useState('');
    const isAuth = useSelector(state => state.auth.isAuth);
    const totalQuantity = useSelector(state => state.cart.totalQuantity);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const logoutHandler = useCallback(() => {
        dispatch(authActions.logout());
        navigate('/login', { replace: true });
    }, [dispatch, navigate]);

    useEffect(() => {
        if (totalQuantity !== 0) {
            setAnimation('pulse 2s infinite');
        } else {
            setAnimation('');
        }
    }, [totalQuantity]);

    return (
        <header className={s.header}>
            <div className={s.title}>ReduxCart</div>
            {isAuth &&
                <div className={s.logInfo}>
                    <nav className={s.nav}>
                        <ul>
                            <li><a className={s.nav__link} href="#">My orders</a></li>
                            <li><a className={s.nav__link} href="#">Profile</a></li>
                        </ul>
                    </nav>
                    <div className={s.actions}>
                        <Button text={'Logout'} onClick={logoutHandler} />
                        <Link className={s['cart-wrapper']} to='/cart' style={{ textDecoration: 'none' }}>
                            <div className={s.cart}>Cart</div>
                            <div className='amount' style={{ animation: animation }}>{totalQuantity}</div>
                        </Link>
                    </div>
                </div>
            }
        </header>
    )
}) 