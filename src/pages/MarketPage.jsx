import { Market } from "../components/Market/Market"

export const MarketPage = () => {
    return (
        <>
            <div className="gradient-bg-2"></div>
            <div className="gradient-bg-3"></div>
            <div className='gradient-bg-4'></div>
            <div className='gradient-bg-5'></div>
            <div className='gradient-bg-6'></div>
            <Market />
        </>
    )

}